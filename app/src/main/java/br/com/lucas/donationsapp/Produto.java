package br.com.lucas.donationsapp;

/**
 * Created by alana on 17/11/2017.
 */

public class Produto {

    private int id;
    private String name;
    private String description;
    private String path_image;
    private String user_name;
    private String user_email;
    private String user_phone;
    private int cities_id;

    public Produto(int id, String name, String description, String path_image, String user_name, String user_email, String user_phone, int cities_id) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.path_image = path_image;
        this.user_name = user_name;
        this.user_email = user_email;
        this.user_phone = user_phone;
        this.cities_id = cities_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPath_image() {
        return path_image;
    }

    public void setPath_image(String path_image) {
        this.path_image = path_image;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUser_phone() {
        return user_phone;
    }

    public void setUser_phone(String user_phone) {
        this.user_phone = user_phone;
    }

    public int getCities_id() {
        return cities_id;
    }

    public void setCities_id(int cities_id) {
        this.cities_id = cities_id;
    }
}

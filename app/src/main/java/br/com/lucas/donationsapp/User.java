package br.com.lucas.donationsapp;

public class User {
    public long id;
    public String name;
    public String email;
    public String phone;

    public User(long id, String name, String email, String phone) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.phone = phone;
    }
}
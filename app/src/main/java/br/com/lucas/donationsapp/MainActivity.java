package br.com.lucas.donationsapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;

import com.squareup.picasso.Picasso;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView imgLogo;
    private Spinner spnCidade;
    private Button btnPesquisar;
    public static final String TAG = MainActivity.class.getSimpleName();
    public static final String SITE_KEY = "6LdnXzwUAAAAAH3WDWsjxRrjAQ2lUwt_k3rGOiD9";
    public static final String SITE_SECRET_KEY = "6LdnXzwUAAAAANT30SWeo93atqAZVkSyuVZaNB6T";
    String userResponseToken;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imgLogo = (ImageView) findViewById(R.id.imgLogo);
        spnCidade = (Spinner) findViewById(R.id.spnCidade);
        btnPesquisar = (Button) findViewById(R.id.btnPesquisar);

        btnPesquisar.setOnClickListener(this);

        String pathFotos = "http://10.0.2.2/donations-api/images/";
        Picasso.with(this).load(pathFotos + "logo.png").into(imgLogo);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item);

        adapter.add("Pelotas");
        adapter.add("Camaquã");
        adapter.add("São Lourenço");

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spnCidade.setAdapter(adapter);
    }

    @Override
    public void onClick(View view) {
        String cidade = spnCidade.getSelectedItem().toString();

        Intent it = new Intent(this, ProdutoListaActivity.class);
        startActivity(it);
    }
}
package br.com.lucas.donationsapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by alana on 17/11/2017.
 */

public class ProdutosAdapter extends BaseAdapter {
    private Context ctx;
    private List<Produto> produtos;
    private String pathFotos;

    public ProdutosAdapter(Context ctx, List<Produto> produtos, String pathFotos) {
        this.ctx = ctx;
        this.produtos = produtos;
        this.pathFotos = pathFotos;
    }

    @Override
    public int getCount() {
        return produtos.size();
    }

    @Override
    public Object getItem(int i) {
        return produtos.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        Produto produto = produtos.get(i);
        View linha = LayoutInflater.from(ctx).inflate(R.layout.activity_produto_item, null);

        ImageView imgFoto = (ImageView) linha.findViewById(R.id.imgFoto);
        TextView txtNome = (TextView) linha.findViewById(R.id.txtNome);
        TextView txtDescricao = (TextView) linha.findViewById(R.id.txtDescricao);

        Picasso.with(ctx).load(produto.getPath_image()).into(imgFoto);
        txtNome.setText(produto.getName());
        txtDescricao.setText(produto.getDescription());

        return linha;
    }
}

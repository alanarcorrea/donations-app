package br.com.lucas.donationsapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class UserDB extends SQLiteOpenHelper {

    public UserDB(Context context) {
        super(context, "donations.sqlite", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table if not exists users(_id integer primary key autoincrement, name text, email text, phone text);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public User search(int id) {
        SQLiteDatabase db = getReadableDatabase();

        try {
            Cursor c = db.query("users", null, "_id=?", new String[]{String.valueOf(id)}, null, null, null);

            // Verifica se encontrou o registro
            if (c.getCount() > 0) {
                c.moveToFirst();

                // getString(nº da coluna a ser recuperada)
                String name = c.getString(1);
                String email = c.getString(2);
                String phone = c.getString(3);

                return new User(id, name, email, phone);
            } else {
                return new User(0, "", "", "");
            }

        } finally {
            db.close();
        }
    }

    public long save(User user) {
        SQLiteDatabase db = getWritableDatabase();

        try {
            ContentValues values = new ContentValues();

            values.put("name", user.name);
            values.put("email", user.email);
            values.put("phone", user.phone);

            long id = db.insert("users", "", values);

            return id;
        } finally {
            db.close();
        }
    }
}
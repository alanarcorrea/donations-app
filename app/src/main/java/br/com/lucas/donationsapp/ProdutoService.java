package br.com.lucas.donationsapp;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by alana on 17/11/2017.
 */

public interface ProdutoService {
    @GET("products")
    Call<List<Produto>> getProdutos();

    @GET("proposals/{product_id}")
    Call<List<Proposta>> getPropostas(@Path("product_id") int product_id);

    @FormUrlEncoded
    @POST("products/new")
    Call<Produto> novoProduto(@Field("name") String name,
                              @Field("description") String description,
                              @Field("user_name") String userName,
                              @Field("user_email") String userEmail,
                              @Field("user_phone") String userPhone,
                              @Field("path_image") String path_image,
                              @Field("cities_id") int cities_id);

    @FormUrlEncoded
    @POST("proposals/new")
    Call<Proposta> enviaProposta(@Field("user_name") String nome,
                         @Field("user_email") String email,
                         @Field("user_phone") String telefone,
                         @Field("description") String descricao,
                         @Field("product_id") int produto_id);
}
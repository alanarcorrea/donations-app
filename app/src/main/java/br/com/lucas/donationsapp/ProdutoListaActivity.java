package br.com.lucas.donationsapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ProdutoListaActivity extends AppCompatActivity {

    private ListView lvProdutos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_produto_lista);

        //declarando a ListView
        lvProdutos = (ListView) findViewById(R.id.lvProdutos);

        //formatando a ListVIew com Retrofit
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://10.0.2.2:8000/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ProdutoService service = retrofit.create(ProdutoService.class);

        Call<List<Produto>> produtos = service.getProdutos();

        produtos.enqueue(new Callback<List<Produto>>() {
            @Override
            public void onResponse(Call<List<Produto>> call, Response<List<Produto>> response) {

                if (response.isSuccessful()) {
                    List<Produto> listaProdutos = response.body();

                    ProdutosAdapter adapter = new ProdutosAdapter(
                            getApplicationContext(),
                            listaProdutos,
                            null
                    );

                    lvProdutos.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<List<Produto>> call, Throwable t) {

            }
        });

        lvProdutos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            Produto produto = (Produto) adapterView.getItemAtPosition(i);

            Intent it = new Intent(getApplicationContext(), ProdutoActivity.class);

            it.putExtra("id", "" + produto.getId());
            it.putExtra("nome", produto.getName());
            it.putExtra("descricao", produto.getDescription());
            it.putExtra("path_image", produto.getPath_image());
            it.putExtra("user_name", produto.getUser_name());
            it.putExtra("user_email", produto.getUser_email());
            it.putExtra("user_phone", produto.getUser_phone());

            startActivity(it);
            }
        });
    }

    public void sellProduct(View view) {
        Intent it = new Intent(this, NewProductActivity.class);
        startActivity(it);
    }
}
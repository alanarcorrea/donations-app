package br.com.lucas.donationsapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ProdutoActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView imgProduto;
    private TextView txtNome;
    private TextView txtDescricao;
    private TextView txtUserName;
    private TextView txtUserEmail;
    private TextView txtUserPhone;
    private EditText edtNome;
    private EditText edtEmail;
    private EditText edtTelefone;
    private EditText edtProposta;
    private Button btnEnviar;
    private Button btnPropostas;
    private EditText productId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_produto);

        imgProduto = (ImageView) findViewById(R.id.imgProduto);
        txtNome = (TextView) findViewById(R.id.txtNome);
        txtDescricao = (TextView) findViewById(R.id.txtDescricao);
        txtUserName = (TextView) findViewById(R.id.txtUserName);
        txtUserEmail = (TextView) findViewById(R.id.txtUserEmail);
        txtUserPhone = (TextView) findViewById(R.id.txtUserPhone);
        edtNome = (EditText) findViewById(R.id.edtNome);
        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtTelefone = (EditText) findViewById(R.id.edtTelefone);
        edtProposta = (EditText) findViewById(R.id.edtProposta);
        btnEnviar = (Button) findViewById(R.id.btnEnviar);
        btnPropostas = (Button) findViewById(R.id.btnPropostas);
        productId = (EditText) findViewById(R.id.productId);

        Intent it = getIntent();
        String id = it.getStringExtra("id");
        String nome = it.getStringExtra("nome");
        String descricao = it.getStringExtra("descricao");
        String path_image = it.getStringExtra("path_image");
        String user_name = it.getStringExtra("user_name");
        String user_email = it.getStringExtra("user_email");
        String user_phone = it.getStringExtra("user_phone");

        Picasso.with(this).load(path_image).into(imgProduto);
        txtNome.setText("Produto: " + nome);
        txtDescricao.setText("Descrição: " + descricao);
        txtUserName.setText("Nome: " + user_name);
        txtUserEmail.setText("E-mail: " + user_email);
        txtUserPhone.setText("Telefone: " + user_phone);
        productId.setText(id);


        UserDB userDB = new UserDB(this);
        User user = userDB.search(1);

        if (user.id > 0) {
            Toast.makeText(this, "UISDAHASD: " + user.id, Toast.LENGTH_LONG ).show();

            edtNome.setText(user.name);
            edtEmail.setText(user.email);
            edtTelefone.setText(user.phone);
        }

        btnEnviar.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent it = getIntent();
        String str_produto_id = it.getStringExtra("id");
        final String nome = edtNome.getText().toString();
        final String email = edtEmail.getText().toString();
        final String telefone = edtTelefone.getText().toString();
        String proposta = edtProposta.getText().toString();

        if (nome.trim().isEmpty() || email.trim().isEmpty() || proposta.trim().isEmpty() || telefone.trim().isEmpty()) {
            Toast.makeText(this, "Informe os dados da proposta", Toast.LENGTH_LONG).show();
            edtNome.requestFocus();
            return;
        }

        int produto_id = Integer.parseInt(str_produto_id);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://10.0.2.2:8000/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ProdutoService service = retrofit.create(ProdutoService.class);
        Call<Proposta> propostaCall = service.enviaProposta(nome, email, telefone, proposta, produto_id);

        propostaCall.enqueue(new Callback<Proposta>() {
            @Override
            public void onResponse(Call<Proposta> call, Response<Proposta> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Proposta enviada com sucesso!", Toast.LENGTH_LONG).show();


                    UserDB userDB = new UserDB(getApplicationContext());
                    User user = userDB.search(1);

                    if (user.id == 0) {
                        long userId = userDB.save(new User(0, nome, email, telefone));
                    }

                    edtNome.setText("");
                    edtEmail.setText("");
                    edtTelefone.setText("");
                    edtProposta.setText("");
                    edtNome.requestFocus();
                }
            }

            @Override
            public void onFailure(Call<Proposta> call, Throwable t) {

            }
        });
    }

    public void verPropostas(View view) {
        //Toast.makeText(this, "PRODUCT ID " + productId.getText().toString(), Toast.LENGTH_SHORT).show();


        Intent it = new Intent(getApplicationContext(), PropostaActivity.class);

        it.putExtra("product_id", "" + productId.getText().toString());

        startActivity(it);
    }

    public void backPage(View view) {
        Intent it = new Intent(this, ProdutoListaActivity.class);
        startActivity(it);
    }
}

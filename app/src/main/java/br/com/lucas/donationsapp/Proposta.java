package br.com.lucas.donationsapp;

/**
 * Created by alana on 17/11/2017.
 */

public class Proposta {

    private int id;
    private String user_name;
    private String user_email;
    private String user_phone;
    private String description;
    private int product_id;

    public Proposta(int id, String user_name, String user_email, String user_phone, String description, int product_id) {
        this.id = id;
        this.user_name = user_name;
        this.user_email = user_email;
        this.user_phone = user_phone;
        this.description = description;
        this.product_id = product_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUser_phone() {
        return user_phone;
    }

    public void setUser_phone(String user_phone) {
        this.user_phone = user_phone;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }
}

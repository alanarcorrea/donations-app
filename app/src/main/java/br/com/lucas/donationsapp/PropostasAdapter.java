package br.com.lucas.donationsapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by alana on 18/11/2017.
 */

public class PropostasAdapter extends BaseAdapter {
    private Context ctx;
    private List<Proposta> propostas;

    public PropostasAdapter(Context ctx, List<Proposta> propostas) {
        this.ctx = ctx;
        this.propostas = propostas;
    }

    @Override
    public int getCount() {
        return propostas.size();
    }

    @Override
    public Object getItem(int i) {
        return propostas.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Proposta proposta= propostas.get(i);
        View linha = LayoutInflater.from(ctx).inflate(R.layout.activity_proposta_item, null);

        TextView txtNome = (TextView) linha.findViewById(R.id.txtNome);
        TextView txtEmail = (TextView) linha.findViewById(R.id.txtEmail);
        TextView txtProposta= (TextView) linha.findViewById(R.id.txtProposta);

        txtNome.setText("Nome: " + proposta.getUser_name());
        txtEmail.setText("E-mail: " + proposta.getUser_email());
        txtProposta.setText("Proposta: " + proposta.getDescription());

        return linha;
    }
}
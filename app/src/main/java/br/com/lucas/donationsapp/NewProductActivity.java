package br.com.lucas.donationsapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.safetynet.SafetyNet;
import com.google.android.gms.safetynet.SafetyNetApi;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NewProductActivity extends AppCompatActivity {

    private EditText edtProductName;
    private EditText edtProductDescription;
    private EditText edtPathImage;
    private EditText edtUserName;
    private EditText edtUserEmail;
    private EditText edtUserPhone;
    public static final String TAG = MainActivity.class.getSimpleName();
    public static final String SITE_KEY = "6LdnXzwUAAAAAH3WDWsjxRrjAQ2lUwt_k3rGOiD9";
    public static final String SITE_SECRET_KEY = "6LdnXzwUAAAAANT30SWeo93atqAZVkSyuVZaNB6T";
    String userResponseToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_product);

        edtProductName = (EditText) findViewById(R.id.edtProductName);
        edtProductDescription = (EditText) findViewById(R.id.edtProductDescription);
        edtPathImage = (EditText) findViewById(R.id.edtPathImage);
        edtUserName = (EditText) findViewById(R.id.edtUserName);
        edtUserEmail = (EditText) findViewById(R.id.edtUserEmail);
        edtUserPhone = (EditText) findViewById(R.id.edtUserPhone);
    }

    public void newProduct() {
        String productName = edtProductName.getText().toString();
        String productDescription = edtProductDescription.getText().toString();
        String pathImage = edtPathImage.getText().toString();
        String userName = edtUserName.getText().toString();
        String userEmail = edtUserEmail.getText().toString();
        String userPhone = edtUserPhone.getText().toString();

        if (
            productName.trim().isEmpty() ||
            productDescription.trim().isEmpty() ||
            pathImage.trim().isEmpty() ||
            userName.trim().isEmpty() ||
            userEmail.trim().isEmpty() ||
            userPhone.trim().isEmpty()
         ) {
            Toast.makeText(this, "Informe os dados corretamente", Toast.LENGTH_LONG).show();
            edtProductName.requestFocus();
            return;
        }

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://10.0.2.2:8000/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ProdutoService service = retrofit.create(ProdutoService.class);
        Call<Produto> produtoCall = service.novoProduto(productName, productDescription, userName, userEmail, userPhone, pathImage, 1);

        produtoCall.enqueue(new Callback<Produto>() {
            @Override
            public void onResponse(Call<Produto> call, Response<Produto> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Produto cadastrado com sucesso!", Toast.LENGTH_LONG).show();

                    edtProductName.setText("");
                    edtProductDescription.setText("");
                    edtPathImage.setText("");
                    edtUserName.setText("");
                    edtUserEmail.setText("");
                    edtUserPhone.setText("");
                    edtProductName.requestFocus();

                    Intent it = new Intent(getApplicationContext(), ProdutoActivity.class);
                    it.putExtra("id", "" + response.body().getId());
                    it.putExtra("nome", response.body().getName());
                    it.putExtra("descricao", response.body().getDescription());
                    it.putExtra("path_image", response.body().getPath_image());
                    it.putExtra("user_name", response.body().getUser_name());
                    it.putExtra("user_email", response.body().getUser_email());
                    it.putExtra("user_phone", response.body().getUser_phone());
                    it.putExtra("product_id", "" + response.body().getId());

                    startActivity(it);
                }
            }

            @Override
            public void onFailure(Call<Produto> call, Throwable t) {

            }
        });
    }



    public void sendRequest()  {

        String url = "https://www.google.com/recaptcha/api/siteverify";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            //Toast.makeText(MainActivity.this, obj.getString("success"), Toast.LENGTH_LONG).show();
                            if (obj.getString("success").equals("true")){
                                newProduct();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(NewProductActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("secret", SITE_SECRET_KEY);
                params.put("response", userResponseToken);
                return params;
            }
        };
        AppController.getInstance(this).addToRequestQueue(stringRequest);
    }


    public void recaptchaClick(View view) {
        SafetyNet.getClient(this).verifyWithRecaptcha(SITE_KEY)
                .addOnSuccessListener(this, new OnSuccessListener<SafetyNetApi.RecaptchaTokenResponse>() {
                    @Override
                    public void onSuccess(SafetyNetApi.RecaptchaTokenResponse response) {
                        userResponseToken= response.getTokenResult();
                        if (!userResponseToken.isEmpty()) {
                            sendRequest();
                        }
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            Log.d(TAG, "Error message: " +
                                    CommonStatusCodes.getStatusCodeString(apiException.getStatusCode()));
                        } else {
                            Log.d(TAG, "Unknown type of error: " + e.getMessage());
                        }
                    }
                });
    }

    public void backPage(View view) {
        Intent it = new Intent(this, ProdutoListaActivity.class);
        startActivity(it);
    }
}
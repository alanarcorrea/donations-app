package br.com.lucas.donationsapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PropostaActivity extends AppCompatActivity {

    private ListView lvPropostas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proposta);

        lvPropostas = (ListView) findViewById(R.id.lvPropostas);

        Intent it = getIntent();
        String product_id = it.getStringExtra("product_id");

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://10.0.2.2:8000/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ProdutoService service = retrofit.create(ProdutoService.class);

        Call<List<Proposta>> propostas = service.getPropostas(Integer.parseInt(product_id));

        propostas.enqueue(new Callback<List<Proposta>>() {
            @Override
            public void onResponse(Call<List<Proposta>> call, Response<List<Proposta>> response) {

                if (response.isSuccessful()) {
                    List<Proposta> listaPropostas= response.body();

                    PropostasAdapter adapter = new PropostasAdapter(
                            getApplicationContext(),
                            listaPropostas
                    );

                    lvPropostas.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<List<Proposta>> call, Throwable t) {

            }
        });
    }
}